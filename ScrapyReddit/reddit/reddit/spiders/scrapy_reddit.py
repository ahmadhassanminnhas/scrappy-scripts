import scrapy


class QuotesSpider(scrapy.Spider):
    name = "reddit"

    def start_requests(self):
        urls = [
            'http://reddit.com/r/python'
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        print(response.xpath('//a[@class="SQnoC3ObvgnGjWt90zD9Z _2INHSNB8V5eaWp4P0rY_mE"]/@href').getall())
